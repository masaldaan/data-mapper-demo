package com.xnsio.source.simplesource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

@RestController
public class DataController {
    @RequestMapping("/{file}")
    public String fetchFile(@PathVariable String file) throws IOException, URISyntaxException {
        return Files.readString(Paths.get(getClass().getClassLoader()
                .getResource(file + ".json")
                .toURI()));
    }
}

