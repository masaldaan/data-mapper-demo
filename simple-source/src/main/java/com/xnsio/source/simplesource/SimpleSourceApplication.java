package com.xnsio.source.simplesource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleSourceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleSourceApplication.class, args);
	}

}
