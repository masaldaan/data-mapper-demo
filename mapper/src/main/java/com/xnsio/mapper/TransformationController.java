package com.xnsio.mapper;

import com.xnsio.mapper.models.TransformedMovie;
import com.xnsio.mapper.source.SimpleSourceService;
import com.xnsio.mapper.tracing.TraceMe;
import com.xnsio.mapper.transform.ConfigurableTransform;
import com.xnsio.mapper.transform.CrudeTransform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TransformationController {
    @Autowired
    ConfigurableTransform configurableTransform;

    @Autowired
    CrudeTransform crudeTransform;

    @Autowired
    SimpleSourceService source;

    @TraceMe
    @RequestMapping("/crude/{file}")
    public List<TransformedMovie> crude(@PathVariable String file) {
        return crudeTransform.transform(source.fetch(file));
    }

    @TraceMe
    @RequestMapping("/configurable/{file}")
    public String configurable(@PathVariable String file) {
        return configurableTransform.transform(source.fetch(file));
    }
}
