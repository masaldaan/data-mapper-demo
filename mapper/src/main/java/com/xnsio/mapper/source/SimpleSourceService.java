package com.xnsio.mapper.source;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SimpleSourceService {
    public String fetch(String file){
        return new RestTemplate().getForObject("http://localhost:8082/{file}",
                String.class, file);
    }
}
