package com.xnsio.mapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MapperApplication {

	public static void main(String[] args) {
		SpringApplication.run(MapperApplication.class, args);
	}

	// Cleanup code
	// Use work order example instead

	// Validation for data, range etc?
	// On startup, pull from data store
	// Add endpoint to put new spec
	// Auto-increment version on push of new version
	// Non json? XML/gRPC etc
}
