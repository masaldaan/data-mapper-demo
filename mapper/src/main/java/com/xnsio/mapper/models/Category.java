package com.xnsio.mapper.models;

public enum Category {
    Romance, Action, Horror, Comedy, Short, Drama, Western, Documentary
}
