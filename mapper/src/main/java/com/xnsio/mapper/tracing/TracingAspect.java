package com.xnsio.mapper.tracing;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static java.lang.Runtime.getRuntime;
import static java.lang.System.nanoTime;

@Component
@Aspect
public class TracingAspect {
    Logger logger = LoggerFactory.getLogger(TracingAspect.class);

    @Pointcut("execution(@com.xnsio.mapper.tracing.TraceMe * *(..))")
    public void isAnnotated() {}

    @Around("isAnnotated()")
    public Object trace(ProceedingJoinPoint joinPoint) throws Throwable {
        Runtime runtime = getRuntime();
        System.gc();
        Thread.sleep(5000);
        long startTime = nanoTime();
        long initialMemoryUsed = memUsed(runtime);

        Object returnVal = joinPoint.proceed();

        long timeElapsed = nanoTime() - startTime;
        long afterMemoryUsed = memUsed(runtime);
        logger.info("Mem consumed for {} (B:{}) & (A:{}): {}MB", joinPoint.getSignature().getName(), initialMemoryUsed, afterMemoryUsed, (afterMemoryUsed - initialMemoryUsed) / (1024 * 1024));
        logger.info("Execution time for {} in milliseconds: {}", joinPoint.getSignature().getName(), timeElapsed / 1000000);
        return returnVal;
    }

    private static long memUsed(Runtime runtime) {
        return runtime.totalMemory() - runtime.freeMemory();
    }
}
