package com.xnsio.mapper.transform;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xnsio.mapper.models.Category;
import com.xnsio.mapper.models.Movie;
import com.xnsio.mapper.models.TransformedMovie;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class CrudeTransform {

    public List<TransformedMovie> transform(String input) {
        try {
            return convert(new ObjectMapper().readValue(input, new TypeReference<List<Movie>>(){}));
        } catch (IOException e) {
            throw new SpecException("Unable to load JSON Mapper", e);
        }
    }

    private List<TransformedMovie> convert(List<Movie> movies) {
        List<TransformedMovie> result = new ArrayList<>();
        for(Movie movie : movies) {
            TransformedMovie tx = new TransformedMovie();
            tx.setName(movie.getTitle());
            tx.setReleaseYear(movie.getYear());
            List<Category> categories = new ArrayList<>();
            if (movie.getGenres().contains("Drama"))
                categories.add(Category.Drama);
            else if (movie.getGenres().contains("Adventure"))
                categories.add(Category.Action);
            else if (movie.getGenres().contains("Short"))
                categories.add(Category.Short);
            else if (movie.getGenres().contains("Western"))
                categories.add(Category.Western);
            else if (movie.getGenres().contains("Horror"))
                categories.add(Category.Horror);
            else if (movie.getGenres().contains("Documentary"))
                categories.add(Category.Documentary);
            else if (movie.getGenres().contains("Comedy"))
                categories.add(Category.Comedy);
            tx.setCategories(categories);
            result.add(tx);
        }
        return result;
    }
}
