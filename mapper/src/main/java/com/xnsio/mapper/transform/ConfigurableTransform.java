package com.xnsio.mapper.transform;

import com.bazaarvoice.jolt.Chainr;
import com.bazaarvoice.jolt.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConfigurableTransform {
    // Add version as a param
    Chainr specification;

    @Autowired
    public ConfigurableTransform(Chainr specification) {
        this.specification = specification;
    }

    public String transform(String jsonInput) {

        Object input = JsonUtils.jsonToObject(jsonInput);

        Object output = specification.transform(input);

        return JsonUtils.toJsonString(output);
    }

}
