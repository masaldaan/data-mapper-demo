package com.xnsio.mapper.transform;

import org.springframework.core.NestedRuntimeException;

public class SpecException extends NestedRuntimeException {
    public SpecException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
