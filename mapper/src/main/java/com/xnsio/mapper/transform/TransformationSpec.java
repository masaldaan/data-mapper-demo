package com.xnsio.mapper.transform;

import com.bazaarvoice.jolt.Chainr;
import com.bazaarvoice.jolt.JsonUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class TransformationSpec {
    @Bean
    public Chainr specification() throws SpecException {
        try {
            return Chainr.fromSpec(JsonUtils.jsonToList(Files.readString(Paths.get(getClass().getClassLoader()
                    .getResource("spec.json")
                    .toURI()))));
        } catch (IOException | URISyntaxException e) {
            throw new SpecException("Mangled specification", e);
        }
    }

}
