Feature: Test Transform API
  Scenario: Fetch data from crude & configurable transforms & compare
    Given url 'http://localhost:8081/crude/minimal'
    When method GET
    Then status 200
    And assert response[0].name == 'Boarding School Girls\' Pajama Parade'
    * def crudeResponse = response
#    * match crudeResponse[0].name == 'After Dark in Central Park!!!'
#    And assert response.length == 2
#    And match response[0].name == 'FirstUser'
    Given url 'http://localhost:8081/configurable/minimal'
    When method GET
    Then status 200
    And assert response[0].name == 'Boarding School Girls\' Pajama Parade'
    * def configurableResponse = response
    * print crudeResponse
    * print configurableResponse
    And match crudeResponse == configurableResponse